def do_palindrome(l):
	return l[::-1]

def list_to_pairslist(l):
	result = []
	for i in range(len(l)-1):
		ir = do_palindrome(l[i])
		for j in range(i+1,len(l)):
			if ir == l[j]:
				result.append((l[i], l[j]))
	return result

l = input().split()
print(list_to_pairslist(l))

