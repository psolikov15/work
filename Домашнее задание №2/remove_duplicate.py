def remove_duplicate(l):
	return [x for i, x in enumerate(l) if l.index(x) == i]
