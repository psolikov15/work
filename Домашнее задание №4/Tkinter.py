if __name__ == "__main__":
    # gifdir = "/home/pavel/Downloads/"
    gifdir = ''
    from tkinter import *
    from random import shuffle
    from wand.image import Image

    root = Tk()


    def check(l):
        inv = 0
        for i in range(9):
            if (l[i][0] != 0):
                for j in range(i):
                    if (l[j][0] > l[i][0]):
                        inv += 1
        for i in range(9):
            if (l[i][0] == 0):
                inv += 1 + i / 4

        return inv % 2


    l = []
    func = []
    for j in range(9):
        with Image(filename=gifdir + str(j) + '.gif') as img:
            with img.clone() as i:
                i.resize(int(i.width * 1.5), int(i.height * 1.5))
                i.save(filename=gifdir + 'new' + str(j) + '.gif')
    gifs = [PhotoImage(file=gifdir + "new0.gif"),
            PhotoImage(file=gifdir + "new1.gif"),
            PhotoImage(file=gifdir + "new2.gif"),
            PhotoImage(file=gifdir + "new3.gif"),
            PhotoImage(file=gifdir + "new4.gif"),
            PhotoImage(file=gifdir + "new5.gif"),
            PhotoImage(file=gifdir + "new6.gif"),
            PhotoImage(file=gifdir + "new7.gif"),
            PhotoImage(file=gifdir + "new8.gif")]
    sh = list(enumerate(gifs.copy()))
    shuffle(sh)
    while (check(sh) != 0):
        shuffle(sh)


    def search(l, index):
        for i in range(len(l)):
            if str(l[i][0]["image"]) == str(gifs[index]):
                return i


    def search_null(l):
        for i in range(len(l)):
            if l[i][1] == 0:
                return i
        return -1



    def Move0():
        if (search_null(l) != -1):
            if (l[1][1] == 0):
                l[0][0].grid_remove()
                l[1][0].grid()
                l[1][1] = 1
                l[0][1] = 0
                t = l[0][0]["image"]
                l[0][0]["image"] = l[1][0]["image"]
                l[1][0]["image"] = t
            if (l[3][1] == 0):
                l[0][0].grid_remove()
                l[3][0].grid()
                l[3][1] = 1
                l[0][1] = 0
                t = l[0][0]["image"]
                l[0][0]["image"] = l[3][0]["image"]
                l[3][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()


    def Move1():
        if (search_null(l) != -1):
            if (l[2][1] == 0):
                l[1][0].grid_remove()
                l[2][0].grid()
                l[2][1] = 1
                l[1][1] = 0
                t = l[2][0]["image"]
                l[2][0]["image"] = l[1][0]["image"]
                l[1][0]["image"] = t

            if (l[0][1] == 0):
                l[1][0].grid_remove()
                l[0][0].grid()
                l[0][1] = 1
                l[1][1] = 0
                t = l[0][0]["image"]
                l[0][0]["image"] = l[1][0]["image"]
                l[1][0]["image"] = t
            if (l[4][1] == 0):
                l[1][0].grid_remove()
                l[4][0].grid()
                l[4][1] = 1
                l[1][1] = 0
                t = l[4][0]["image"]
                l[4][0]["image"] = l[1][0]["image"]
                l[1][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move2():
        if (search_null(l) != -1):
            if (l[1][1] == 0):
                l[2][0].grid_remove()
                l[1][0].grid()
                l[1][1] = 1
                l[2][1] = 0
                t = l[2][0]["image"]
                l[2][0]["image"] = l[1][0]["image"]
                l[1][0]["image"] = t
            if (l[5][1] == 0):
                l[2][0].grid_remove()
                l[5][0].grid()
                l[5][1] = 1
                l[2][1] = 0
                t = l[5][0]["image"]
                l[5][0]["image"] = l[2][0]["image"]
                l[2][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move3():
        print(search_null(l))
        if (search_null(l) != -1):
            print(l[0][1])
            if (l[0][1] == 0):
                l[3][0].grid_remove()
                l[0][0].grid()
                l[0][1] = 1
                l[3][1] = 0
                t = l[0][0]["image"]
                l[0][0]["image"] = l[3][0]["image"]
                l[3][0]["image"] = t
            if (l[6][1] == 0):
                l[3][0].grid_remove()
                l[6][0].grid()
                l[6][1] = 1
                l[3][1] = 0
                t = l[3][0]["image"]
                l[3][0]["image"] = l[6][0]["image"]
                l[6][0]["image"] = t
            if (l[4][1] == 0):
                l[3][0].grid_remove()
                l[4][0].grid()
                l[4][1] = 1
                l[3][1] = 0
                t = l[3][0]["image"]
                l[3][0]["image"] = l[4][0]["image"]
                l[4][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move4():
        if (search_null(l) != -1):
            if (l[1][1] == 0):
                l[4][0].grid_remove()
                l[1][0].grid()
                l[1][1] = 1
                l[4][1] = 0
                t = l[4][0]["image"]
                l[4][0]["image"] = l[1][0]["image"]
                l[1][0]["image"] = t
            if (l[5][1] == 0):
                l[4][0].grid_remove()
                l[5][0].grid()
                l[5][1] = 1
                l[4][1] = 0
                t = l[4][0]["image"]
                l[4][0]["image"] = l[4][0]["image"]
                l[5][0]["image"] = t
            if (l[3][1] == 0):
                l[4][0].grid_remove()
                l[3][0].grid()
                l[3][1] = 1
                l[4][1] = 0
                t = l[4][0]["image"]
                l[4][0]["image"] = l[3][0]["image"]
                l[3][0]["image"] = t
            if (l[7][1] == 0):
                l[4][0].grid_remove()
                l[7][0].grid()
                l[7][1] = 1
                l[4][1] = 0
                t = l[4][0]["image"]
                l[4][0]["image"] = l[7][0]["image"]
                l[7][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move5():
        if (search_null(l) != -1):
            if (l[2][1] == 0):
                l[5][0].grid_remove()
                l[2][0].grid()
                l[2][1] = 1
                l[5][1] = 0
                t = l[5][0]["image"]
                l[5][0]["image"] = l[2][0]["image"]
                l[2][0]["image"] = t
            if (l[4][1] == 0):
                l[5][0].grid_remove()
                l[4][0].grid()
                l[4][1] = 1
                l[5][1] = 0
                t = l[5][0]["image"]
                l[5][0]["image"] = l[4][0]["image"]
                l[4][0]["image"] = t
            if (l[8][1] == 0):
                l[5][0].grid_remove()
                l[8][0].grid()
                l[8][1] = 1
                l[5][1] = 0
                t = l[5][0]["image"]
                l[5][0]["image"] = l[8][0]["image"]
                l[8][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move6():
        if (search_null(l) != -1):
            if (l[3][1] == 0):
                l[6][0].grid_remove()
                l[3][0].grid()
                l[3][1] = 1
                l[6][1] = 0
                t = l[6][0]["image"]
                l[6][0]["image"] = l[3][0]["image"]
                l[3][0]["image"] = t
            if (l[7][1] == 0):
                l[6][0].grid_remove()
                l[7][0].grid()
                l[7][1] = 1
                l[6][1] = 0
                t = l[6][0]["image"]
                l[6][0]["image"] = l[7][0]["image"]
                l[7][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move7():
        if (search_null(l) != -1):
            if (l[8][1] == 0):
                l[7][0].grid_remove()
                l[8][0].grid()
                l[8][1] = 1
                l[7][1] = 0
                t = l[7][0]["image"]
                l[7][0]["image"] = l[8][0]["image"]
                l[8][0]["image"] = t
            if (l[6][1] == 0):
                l[7][0].grid_remove()
                l[6][0].grid()
                l[6][1] = 1
                l[7][1] = 0
                t = l[7][0]["image"]
                l[7][0]["image"] = l[6][0]["image"]
                l[6][0]["image"] = t
            if (l[4][1] == 0):
                l[7][0].grid_remove()
                l[4][0].grid()
                l[4][1] = 1
                l[7][1] = 0
                t = l[7][0]["image"]
                l[7][0]["image"] = l[4][0]["image"]
                l[4][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()

    def Move8():
        if (search_null(l) != -1):
            if (l[5][1] == 0):
                l[8][0].grid_remove()
                l[5][0].grid()
                l[5][1] = 1
                l[8][1] = 0
                t = l[8][0]["image"]
                l[8][0]["image"] = l[5][0]["image"]
                l[5][0]["image"] = t
            if (l[7][1] == 0):
                l[8][0].grid_remove()
                l[7][0].grid()
                l[7][1] = 1
                l[8][1] = 0
                t = l[8][0]["image"]
                l[8][0]["image"] = l[7][0]["image"]
                l[7][0]["image"] = t
            if ((str(l[0][0]["image"]) == str(gifs[0])) &
                    (str(l[1][0]["image"]) == str(gifs[1])) &
                    (str(l[2][0]["image"]) == str(gifs[2])) &
                    (str(l[3][0]["image"]) == str(gifs[3])) &
                    (str(l[4][0]["image"]) == str(gifs[4])) &
                    (str(l[5][0]["image"]) == str(gifs[5])) &
                    (str(l[6][0]["image"]) == str(gifs[6])) &
                    (str(l[7][0]["image"]) == str(gifs[7])) &
                    (str(l[8][0]["image"]) == str(gifs[8]))):
                l[search_null(l)][0].grid()
                l[search_null(l)][1] = 1
        else:
            top = Toplevel()
            top.title("Congrats!")
            lable = Label(top, text="Congrats")
            lable.pack()
            button = Button(top, text="Dismiss", command=top.destroy)
            button.pack()
            button.pack()
            button.pack()

    for i in range(9):
        l.append([Button(image=sh[i][1]), 1])
        l[i][0].grid(row=i // 3, column=i % 3)
    l[0][0]["command"] = Move0
    l[1][0]["command"] = Move1
    l[2][0]["command"] = Move2
    l[3][0]["command"] = Move3
    l[4][0]["command"] = Move4
    l[5][0]["command"] = Move5
    l[6][0]["command"] = Move6
    l[7][0]["command"] = Move7
    l[8][0]["command"] = Move8
    l[search(l, 8)][0].grid_remove()
    l[search(l, 8)][1] = 0
    print(sh)
    print(gifs)
    root.mainloop()
